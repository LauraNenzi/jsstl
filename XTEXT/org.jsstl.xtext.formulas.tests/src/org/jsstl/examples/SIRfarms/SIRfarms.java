package org.jsstl.examples.SIRfarms;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

import org.jsstl.core.formula.Formula;
import org.jsstl.core.formula.Signal;
import org.jsstl.core.formula.SignalStatistics;
import org.jsstl.core.formula.jSSTLScript;
import org.jsstl.core.monitor.SpatialBooleanSignal;
import org.jsstl.core.monitor.SpatialQuantitativeSignal;
import org.jsstl.core.space.GraphModel;
import org.jsstl.core.space.Location;
import org.jsstl.io.FolderSignalReader;
import org.jsstl.io.TxtSpatialQuantSat;
import org.jsstl.io.TxtSpatialQuantSignal;
import org.jsstl.xtext.formulas.ScriptLoader;

public class SIRfarms {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		// %%%%%%%%%%  GRAPH  %%%%%%%%% //		
		// Designing the grid
		GraphModel graph = GraphModel.createGrid(10, 10, 1.0);
		// Computing of the distance matrix
		graph.dMcomputation();

	
	// %%%%%%%%% PROPERTY %%%%%%% //		
	// loading the formulas files
	ScriptLoader loader  = new ScriptLoader();
	jSSTLScript script = loader.load("data/spreadInf.sstl");
	Formula phi = script.getFormula("inf");
	// Loading the variables. That we have defined in the formulas files.
	
//	/// %%%%%%%  DATA import %%%%%%%%%%%%/////////
	String [] var = script.getVariables();
	System.out.println(Arrays.toString(var));
	FolderSignalReader readSignal = new FolderSignalReader(graph, var);
	File folder = new  File("Input2/Output1");

	
	Signal signal = readSignal.read(folder);

	signal.transfomTimeStep(100,0.1);
	
//////	/// %%%%%%%  CHECK    %%%%%   /////////
String prop = "inf";
SpatialQuantitativeSignal quantSignal = script.quantitativeCheck(new HashMap<>(), prop, graph, signal);
SpatialBooleanSignal boolSignal = script.booleanCheck(new HashMap<>(), prop, graph, signal);
//SpatialQuantitativeSignal quantSignal = phi.quantitativeCheck(null, graph, signal);
////////	/// %%%%%%%  OUTPUT    %%%%%   /////////
	double [] quantSat =quantSignal.quantSat();
	System.out.println(Arrays.toString(quantSat));
	double [] boolSat =boolSignal.boolSat();
	System.out.println(Arrays.toString(boolSat));	
//	TxtSpatialQuantSignal outDataQuantSignal = new TxtSpatialQuantSignal();
//    outDataQuantSignal.write(quantSignal, "/Users/lauretta/Dropbox/L&Ludo/output/dataQuantSignal.txt");
//	TxtSpatialQuantSat outDataQuant = new TxtSpatialQuantSat();
//	outDataQuant.write(quantSat, "/Users/lauretta/Dropbox/L&Ludo/output/dataSatQuant.txt");
	
//	
/////////////  many RUNS  //////////
////
//	int runs = 100;
//	double endT = 100;
//	double deltat = 0.1;
//	int steps = (int) (endT/deltat)+1;
//	//String text = "";
//	SignalStatistics statistic = new SignalStatistics(graph.getNumberOfLocations(),steps);
//	for ( int j=1 ; j<=runs ; j++) {
//	System.out.println(j);
//	folder = new  File("/Users/lauretta/Dropbox/L&Ludo/Input2/Output"+j);
//	signal = readSignal.read(folder);
//	signal.transfomTimeStep(endT,deltat);
//	SpatialQuantitativeSignal qSignal = script.quantitativeCheck(new HashMap<>(), "inf", graph, signal);
//	statistic.add(qSignal.quantTraj());	
//}
//	double [][] meanTraj = statistic.getAverageTraj();
//	System.out.println(Arrays.toString(meanTraj[0])); 
//	//System.out.println(Arrays.toString(	statistic.getStandardDeviation()));
//	
//	/////  write 
//		String text = "";
//		for (int i=0; i<meanTraj.length;i++) {
//			for (int j = 0; j < meanTraj[0].length; j++) {
//					text += String.format(Locale.US, " %20.10f", meanTraj[i][j]);
//			}
//			text += "\n";
//		}
//		PrintWriter printer = new PrintWriter("/Users/lauretta/Dropbox/L&Ludo/output/meanDataQuantSignal.txt");
//		printer.print(text);
//		printer.close();
	
	
	}	
	
	
}
