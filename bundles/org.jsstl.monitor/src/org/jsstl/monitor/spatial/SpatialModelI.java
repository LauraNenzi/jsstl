/**
 * 
 */
package org.jsstl.monitor.spatial;

import java.util.Collection;
import java.util.Set;
import java.util.function.Predicate;

/**
 * @author loreti
 *
 */
public interface SpatialModelI<L> {

	public Collection<L> localities();
	
	public Set<L> getPoset( L l );
	
	public Set<L> getPreset( L l );
	
	public double weight( L l1 , L l2 );
	
	public Set<L> getLocalities( L l , Predicate<Double> distancePredicate );
	
}
