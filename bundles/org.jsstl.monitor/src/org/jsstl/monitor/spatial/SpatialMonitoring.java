/**
 * 
 */
package org.jsstl.monitor.spatial;

import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import org.jsstl.monitor.signal.DomainFunction;

/**
 * @author loreti
 *
 */
public class SpatialMonitoring<L,D1,D2> {

	
	private final DomainFunction<D2> functions;
	private final SpatialModelI<L> model;
	
	/**
	 * @param functions
	 */
	public SpatialMonitoring(DomainFunction<D2> functions, SpatialModelI<L> model) {
		super();
		this.functions = functions;
		this.model = model;
	}
	
	
	
//	public  SpatialSignal<L, D2> atomic( Function<D1,D2> atomic , ) 

	protected SpatialSignal<L, D2> apply( SpatialSignal<L, D2> phi1 , SpatialSignal<L, D2> phi2 , BiFunction<D2, D2, D2> operator ) {
		SpatialSignal<L, D2> result = new SpatialSignal<>();
		for (Entry<L, D2> e : phi1.entrySet()) {
			result.setValue(e.getKey(), operator.apply(e.getValue(), phi2.getValue(e.getKey())));
		}
		return result;
	}
	
	
	public SpatialSignal<L, D2> and( SpatialSignal<L, D2> phi1 , SpatialSignal<L, D2> phi2 ) {
		return apply( phi1 , phi2 , (x,y) -> functions.conjunction(x, y) );
	}
	
	public SpatialSignal<L, D2> or( SpatialSignal<L, D2> phi1 , SpatialSignal<L, D2> phi2 ) {
		return apply( phi1 , phi2 , (x,y) -> functions.disjunction(x, y) );
	}
	
	public SpatialSignal<L, D2> not( SpatialSignal<L, D2> phi ) {
		SpatialSignal<L, D2> result = new SpatialSignal<>();
		for (Entry<L, D2> e : phi.entrySet()) {
			result.setValue(e.getKey(), functions.negation(e.getValue()));
		}
		return result;
	}
	
	public SpatialSignal<L, D2> somewhere( Predicate<Double> p , SpatialSignal<L, D2> phi ) {
		SpatialSignal<L, D2> result = new SpatialSignal<>();
		for( L l: model.localities()) {
			D2 value = functions.falseValue();
			for( L l2: model.getLocalities(l, p)) {
				value = functions.disjunction(value, phi.getValue(l2));
			}
			result.setValue(l, value);
		}		
		return result;
	}
	
	public SpatialSignal<L, D2> everywhere( Predicate<Double> p , SpatialSignal<L, D2> phi ) {
		SpatialSignal<L, D2> result = new SpatialSignal<>();
		for( L l: model.localities()) {
			D2 value = functions.trueValue();
			for( L l2: model.getLocalities(l, p)) {
				value = functions.conjunction(value, phi.getValue(l2));
			}
			result.setValue(l, value);
		}		
		return result;
	}
	
	public SpatialSignal<L, D2> surround( SpatialSignal<L, D2> phi1 , double d1 , double d2 ,  SpatialSignal<L, D2> phi2 ) {
		SpatialSignal<L, D2> result = new SpatialSignal<>();
		for( L l: model.localities()) {
			result.setValue(
					l, 
					computeSurround(l,
							phi1.set(model.getLocalities(l, d -> d>d2 ) , functions.falseValue()),
							phi2.set(model.getLocalities(l, d -> ((d<d1)||(d>d2)))
									 , functions.falseValue()))
				);
		}		
		return result;
	}



	private D2 computeSurround(L l, SpatialSignal<L, D2> phi1, SpatialSignal<L, D2> phi2) {
		boolean isChanged = true;
		SpatialSignal<L, D2> chi = phi1;
		while (!isChanged) {
			SpatialSignal<L, D2> newChi = new SpatialSignal<>();
			isChanged = false;
			for (Entry<L, D2> e: chi.entrySet()) {
				D2 newValue = surroundFixPointStep(e.getKey(),e.getValue(), chi,phi2);
				isChanged = isChanged||(newValue != e.getValue());
				newChi.setValue(e.getKey(),newValue);
			}
			chi = newChi;
		}
		return chi.getValue(l);
	}



	private D2 surroundFixPointStep(L l,D2 value, SpatialSignal<L, D2> chi, SpatialSignal<L, D2> phi2) {
		D2 result = value;
		for (L l2 : model.getPoset(l)) {
			result = functions.conjunction(result, functions.disjunction(chi.getValue(l2), phi2.getValue(l2))); 
		}
		return result;
	}

}
