/**
 * 
 */
package org.jsstl.monitor.spatial;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author loreti
 *
 */
public class SpatialSignal<L,D> {
	
	private final HashMap<L, D> map;
	
	public SpatialSignal( ) {
		this(new HashMap<>());
	}
	
	protected SpatialSignal(HashMap<L, D> map) {
		this.map = map;
	}

	public D getValue( L l ) {
		return map.get(l);
	}
	
	public void setValue( L l , D value ) {
		map.put(l, value);
	}

	public Set<Entry<L, D>> entrySet() {
		return map.entrySet();
	}
	
	@SuppressWarnings("unchecked")
	public SpatialSignal<L, D> set( Set<L> localities , D value ) {
		HashMap<L, D> newMap = (HashMap<L, D>) map.clone();
		for (L l : localities) {
			newMap.put(l, value);
		}
		return new SpatialSignal<L,D>(newMap);		
 	}
	
}
